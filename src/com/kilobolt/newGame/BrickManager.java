package com.kilobolt.newGame;

import java.util.ArrayList;
import java.util.Random;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class BrickManager
{
	
	public ArrayList<Brick> brickList;
	public ArrayList<Brick> brickList1;
	
	Random rand = new Random();
	
	public BrickManager()
	{
		brickList = new ArrayList<Brick>();
		brickList.add(new Brick(800, 415, 40, 40, 0,-1));
		brickList.add(new Brick(brickList.get(brickList.size()-1).positionX + brickList.get(brickList.size()-1).width -  200, 415, 40, 40, 1,-1));
		brickList.add(new Brick(brickList.get(brickList.size()-1).positionX + brickList.get(brickList.size()-1).width - 200, 415, 40, 40, 2,-1));
		brickList.add(new Brick(brickList.get(brickList.size()-1).positionX + brickList.get(brickList.size()-1).width - 200, 415, 40, 40, 1,-1));
		brickList.add(new Brick(brickList.get(brickList.size()-1).positionX + brickList.get(brickList.size()-1).width - 200, 415, 40, 40, 0,-1));
		
		brickList1 = new ArrayList<Brick>();
		brickList1.add(new Brick(0, 445, 40, 40, 0,1));
		brickList1.add(new Brick(brickList1.get(brickList1.size()-1).positionX + brickList1.get(brickList1.size()-1).width -  200, 445, 40, 40, 1,1));
		brickList1.add(new Brick(brickList1.get(brickList1.size()-1).positionX + brickList1.get(brickList1.size()-1).width - 200, 445, 40, 40, 0,1));
		brickList1.add(new Brick(brickList1.get(brickList1.size()-1).positionX + brickList1.get(brickList1.size()-1).width - 200, 445, 40, 40, 2,1));
		brickList1.add(new Brick(brickList1.get(brickList1.size()-1).positionX + brickList1.get(brickList1.size()-1).width - 200, 445, 40, 40, 1,1));
		
		
	}
	public void Update(float deltaTime, Input input)
	{		
		int index = rand.nextInt(3);
	
		for(int i =0; i < brickList.size(); i++)
		{
			brickList.get(i).Update(deltaTime, input);
		}
		
		for(int i =0; i < brickList.size(); i++)
		{
			if(brickList.get(i).positionX < -40)
			{
				brickList.get(i).positionX = 800;
				brickList.get(i).type = rand.nextInt(3);
			}
		}
		
		
		for(int i =0; i < brickList1.size(); i++)
		{
			brickList1.get(i).Update(deltaTime, input);
		}
		
		for(int i =0; i < brickList1.size(); i++)
		{
			if(brickList1.get(i).positionX > 840)
			{
				brickList1.get(i).positionX = 0;
				//brickList1.get(i).type = rand.nextInt(3);
			}
		}
		
		
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		for(int i =0; i < brickList.size(); i++)
		{
			brickList.get(i).Paint(deltaTime, g);
		}
		
		for(int i =0; i < brickList1.size(); i++)
		{
			brickList1.get(i).Paint(deltaTime, g);
		}
	}

}
