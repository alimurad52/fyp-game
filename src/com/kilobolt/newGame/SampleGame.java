package com.kilobolt.newGame;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Screen;
import com.kilobolt.framework.Sound;
import com.kilobolt.framework.Graphics.ImageFormat;
import com.kilobolt.framework.implementation.AndroidGame;

public class SampleGame extends AndroidGame{

		
	@Override
	public Screen getInitScreen() 
	{					
		Graphics g = getGraphics();
		Assets.player = g.newImage("leader2.png", ImageFormat.ARGB8888);	
		Assets.background = g.newImage("Swastikgamebackground.png", ImageFormat.ARGB8888);
		
		Assets.left = g.newImage("leftBT.png", ImageFormat.ARGB4444);
		Assets.right = g.newImage("rightBT.png", ImageFormat.ARGB4444);
		Assets.up = g.newImage("upBT.png", ImageFormat.ARGB4444);
		Assets.down = g.newImage("downBT.png", ImageFormat.ARGB4444);
		Assets.BTDown = g.newImage("BTDown.png", ImageFormat.ARGB4444);
		Assets.level3Splsh = g.newImage("l3.png", ImageFormat.ARGB4444);
		
		Assets.army1 = g.newImage("armyyy.png", ImageFormat.ARGB4444);
		Assets.army1Selected = g.newImage("armySelected.png", ImageFormat.ARGB4444);
		
		Assets.bulletImage = g.newImage("shotBullet.png", ImageFormat.ARGB4444);
		Assets.bulletButton = g.newImage("bulletButton.png", ImageFormat.ARGB4444);
		Assets.bulletButtonDown = g.newImage("bulletButtonDown.png", ImageFormat.ARGB4444);
		Assets.endGameScreen = g.newImage("end.png", ImageFormat.ARGB4444);
		
		Assets.brick1 = g.newImage("C1.png", ImageFormat.ARGB4444);
		Assets.brick2 = g.newImage("C2.png", ImageFormat.ARGB4444);
		Assets.brick3 = g.newImage("C3.png", ImageFormat.ARGB4444);
		Assets.topJungleImage = g.newImage("jungle.png", ImageFormat.ARGB4444);
		Assets.zombie = g.newImage("z1.png", ImageFormat.ARGB4444);
		
		Assets.zombieMed = g.newImage("blue.png", ImageFormat.ARGB4444);
		
		Assets.bulletback = g.newImage("bulletButton.png", ImageFormat.ARGB4444);
		Assets.ak47 = g.newImage("ak47.png", ImageFormat.ARGB4444);
		Assets.ak47Selected = g.newImage("ak47Selected.png", ImageFormat.ARGB4444);
		Assets.shotGun = g.newImage("shotgun.png", ImageFormat.ARGB4444);
		Assets.shotGunSelected = g.newImage("shotGunSelected.png", ImageFormat.ARGB4444);
		Assets.sniper = g.newImage("sniper.png", ImageFormat.ARGB4444);
		Assets.sniperSelected = g.newImage("sniperSelected.png", ImageFormat.ARGB4444);
		Assets.machineGun = g.newImage("machineGun.png", ImageFormat.ARGB4444);
		Assets.machineGunselected = g.newImage("machineGunSelected.png", ImageFormat.ARGB4444);
		Assets.inst = g.newImage("ins.png", ImageFormat.ARGB4444);
		Assets.ttc = g.newImage("clear.png", ImageFormat.ARGB4444);
		Assets.menuScreen =  g.newImage("menuImage.png", ImageFormat.ARGB4444);
		Assets.level1Splash =  g.newImage("level1splash.png", ImageFormat.ARGB4444);
		Assets.level2Splash =  g.newImage("level2splash.png", ImageFormat.ARGB4444);
		Assets.pauseBT =  g.newImage("pausebt.png", ImageFormat.ARGB4444);
		Assets.resumeScreen =  g.newImage("pauseScreen.png", ImageFormat.ARGB4444);
		Assets.level2House =  g.newImage("houses.png", ImageFormat.ARGB4444);
		Assets.leve3House =  g.newImage("level3House.png", ImageFormat.ARGB4444);
		Assets.level3Back =  g.newImage("level3back.png", ImageFormat.ARGB4444);
		Assets.level3Top =  g.newImage("level3Top.png", ImageFormat.ARGB4444);
		Assets.bigZombie =  g.newImage("brown.png", ImageFormat.ARGB4444);
		Assets.goScreen =  g.newImage("goScreen.png", ImageFormat.ARGB4444);
		
		Assets.level2back =  g.newImage("Swastikgamebackground1.png", ImageFormat.ARGB4444);
		Assets.level2backTop =  g.newImage("jungle.png", ImageFormat.ARGB4444);
		
		Assets.vir =  g.newImage("virus01.png", ImageFormat.ARGB4444);
		Assets.sur =  g.newImage("survivors001.png", ImageFormat.ARGB4444);
		Assets.city =  g.newImage("city view01.png", ImageFormat.ARGB4444);
		Assets.zom =  g.newImage("zombies01.png", ImageFormat.ARGB4444);
		
		Assets.tapSound = getAudio().createSound("button-21.wav");
		Assets.bulletSound = getAudio().createSound("shoot-02.wav");
		Assets.zombiekillSound = getAudio().createSound("spawn-08.wav");
		Assets.tr1 = getAudio().createMusic("track 1.mp3");
		Assets.tr2 = getAudio().createMusic("track2.mp3");
		Assets.tr3 = getAudio().createMusic("track3.mp3");
		Assets.tr4 = getAudio().createMusic("track4.mp3");
		Assets.music1 = getAudio().createMusic("soundtrack1.mp3");
		Assets.music2 = getAudio().createMusic("soundtrack2.mp3");
		Assets.amb = getAudio().createMusic("atmosphere.mp3");
		return  new Level3game(this);
		//return new Level3game(this);
	}
	
	@Override
	public void onBackPressed() {
		getCurrentScreen().backButton();
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
	}
	

	@Override
	public void onPause() {
		super.onPause();		

	}

}
