package com.kilobolt.newGame;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Paint;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class Level2game extends Screen{

	
	PlayerInput pInput;
	Player player;
	background Background;
	ArmyManager armyManager;
	ZombieManager  zm;
	BrickManager bm;
	ArrayList<Vector2D> zombiePos;
	boolean ispaused = false;
	
	
	public Level2game(Game game) 
	{
		super(game);
		
		pInput = new PlayerInput();
		player = new Player(400,300,1);
		Background = new background();
		armyManager = new ArmyManager();
		zombiePos = new ArrayList<Vector2D>();
		
		zombiePos.add(new Vector2D(85,115));
		zombiePos.add(new Vector2D(210,130));
		zombiePos.add(new Vector2D(405,80));
		zombiePos.add(new Vector2D(530,135));
		zombiePos.add(new Vector2D(640,110));
		
		bm = new BrickManager();
		
		zm = new ZombieManager(zombiePos);
		armyManager.armyPositioningDone = false;
		Assets.music2.setVolume(0.5f);
		Assets.music2.play();
		Assets.music2.setLooping(true);
		Assets.music1.stop();
		player.level = 2;
		
		zm.positionsMed.add(new Vector2D(145,0));
		zm.positionsMed.add(new Vector2D(295,0));
		zm.positionsMed.add(new Vector2D(485,0));
		
	}
	
	
	float t =0;
	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		
		
		if(!ispaused)
		{
		pInput.Update(deltaTime, input);
		
		if(armyManager.armyPositioningDone){
		player.Update(deltaTime,input);
		zm.Update(deltaTime, input);
		}
		bm.Update(deltaTime, input);
		armyManager.Update(deltaTime, input);
		
		if(input.isTouchDown(0))
		{
			if( input.getTouchX(0) > 554 && input.getTouchX(0) < 641)
			{	
				if(input.getTouchY(0) > 0 && input.getTouchY(0) < 30)
				{
					armyManager.armyPositioningDone = true;
				
				}				
			}
		}
		
		if(zm.noMoreZombie)
		{
			t += deltaTime/60;
			if(input.isTouchDown(0))
			{
				if(t > 1.50f)
				{
					Assets.tapSound.play(5);
						game.setScreen(new level3Splash(game));
				}
			}
		}
		
		}
		else
		{
			Assets.music2.pause();
			if(input.isTouchDown(0))
			{
				if( input.getTouchX(0) > 118 && input.getTouchX(0) < 271)
				{
					if(input.getTouchY(0) > 148 && input.getTouchY(0) < 190)
					{
						Assets.tapSound.play(5);
						ispaused = false;
						Assets.music2.play();
					}
				}
				
				if( input.getTouchX(0) > 118 && input.getTouchX(0) < 271)
				{
					if(input.getTouchY(0) > 265 && input.getTouchY(0) < 305)
					{
						Assets.tapSound.play(5);
						game.setScreen(new Menuscreen(game));
					}
				}
				
				if( input.getTouchX(0) > 118 && input.getTouchX(0) < 271)
				{
					if(input.getTouchY(0) > 384 && input.getTouchY(0) < 424)
					{
						Assets.tapSound.play(5);
						System.exit(0);
					}
				}
			}
		}
		
		
		if(input.isTouchDown(0))
		{
			if( input.getTouchX(0) > 720 && input.getTouchX(0) < 790)
			{	
				if(input.getTouchY(0) > 5 && input.getTouchY(0) < 50)
				{
					Assets.tapSound.play(5);
					ispaused = true;
				
				}				
			}
		}
		
		
		if(zm.go)
			game.setScreen(new GameOverScreen(game));
	
	}

	
	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		
		if(!ispaused)
		{
		Background.Paint(deltaTime, g);		
		bm.Paint(deltaTime, g);
		pInput.Paint(deltaTime, g);
		player.Paint(deltaTime,g);
		armyManager.Paint(deltaTime, g);
		zm.Paint(deltaTime, g);
		g.drawImage(Assets.level2backTop, 0, 0);
		g.drawImage(Assets.level2House, 0, 0);
		if(!armyManager.armyPositioningDone)
			g.drawImage(Assets.inst,0, 0);
		
		if(zm.noMoreZombie)
			g.drawImage(Assets.ttc, 50, 120);
		
		g.drawImage(Assets.pauseBT, 720,5);
		
		Paint p  = new Paint();
		p.setColor(Color.CYAN);
		p.setTextSize(20);
		g.drawString("Score:" + Player.score, 10, 40, p);
		
		}
		else
		{
			g.drawImage(Assets.resumeScreen, 0, 0);
		}
		
	}
	
	
	

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}


}
