package com.kilobolt.newGame;

import android.graphics.Color;
import android.graphics.Paint;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class Story  extends Screen{

	
	boolean first,second,third,fourth;
	
	public Story(Game game) 
	{
		super(game);
		first = true;
		second = false;
		third = false;
		fourth = false;
		Assets.amb.play();
		Assets.amb.setLooping(true);
		Assets.amb.setVolume(1);
		
		
	}

	float t = 0;	
	int tap = 0;
	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		t += deltaTime/60;
		if(input.isTouchDown(0))
		{
			
			if(t > 1.0f)
			{
				Assets.tapSound.play(5);
				
				tap += 1;
				if(tap >= 4)
				{
					Assets.amb.stop();
					Assets.tr4.stop();
					game.setScreen(new level1Splash(game));
				}
				t = 0;
			}
		}
		
		if(tap == 0)
		{
			first = true;
			second = false;
			third = false;
			fourth = false;
			Assets.tr1.play();
			Assets.tr1.setVolume(10);
		}
		if(tap == 1)
		{
			first = false;
			second = true;
			third = false;
			fourth = false;
			Assets.tr1.stop();
			Assets.tr2.play();
			Assets.tr2.setVolume(10);
		}
		if(tap == 2)
		{
			first = false;
			second = false;
			third = true;
			fourth = false;
			Assets.tr2.stop();
			Assets.tr3.play();
			Assets.tr3.setVolume(10);
		}
		if(tap == 3)
		{
			first = false;
			second = false;
			third = false;
			fourth = true;
			Assets.tr3.stop();
			Assets.tr4.play();
			Assets.tr4.setVolume(10);
		}
	
	}

	
	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		
		if(first)
		{
			g.drawImage(Assets.city, 0, 0);
		}
		 if(second)
		{
			g.drawImage(Assets.vir, 0, 0);
		}
		 if(third)
		{
			g.drawImage(Assets.zom, 0, 0);
		}
		 if(fourth)
		{
			g.drawImage(Assets.sur, 0, 0);			
		}
		
		

			Paint p  = new Paint();
			p.setColor(Color.CYAN);
			p.setTextSize(30);
			g.drawString("Tap to skip/continue to next page", 200, 400, p);
		
	}
	
	
	

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}

}
