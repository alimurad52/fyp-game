package com.kilobolt.newGame;

import android.graphics.Matrix;
import android.graphics.Rect;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class ZombieMed1 {

	public int height;
	public int width;
	private float speed;
	private Matrix matrix;
	private float rotataion;	
	public Vector2D Position;
	private Vector2D targetPosition;
	private Vector2D desiredVel;
	public Rect rect;
	public boolean isAlive = true;
	public float health = 100;
	
	public ZombieMed1(int pX, int pY, float speed)
	{	
		width = 50;
		height = 50;
		
		matrix = new Matrix();
		rotataion = -90;
		Position = new Vector2D(pX,pY);
		desiredVel = new Vector2D();
		targetPosition = new Vector2D(pX,pY + 450);
		this.speed = speed/2;
		rect = new Rect((int)Position.x, (int)Position.y,100, 100);
		
	}
	
	public void Update(float deltaTime, Input input)
	{		
			Vector2D dir = new Vector2D(targetPosition.x - Position.x, targetPosition.y - Position.y);
			float distance  = (float)dir.length();
			float dAngle = (float)dir.getTheta();
			float diff = dAngle * 180/3.14f  - rotataion;
			rotataion += diff;
			dir = dir.unitVector();
			rect = new Rect((int)Position.x, (int)Position.y,(int)Position.x + 50, (int)Position.y + 50);
			if(distance < 80)
			{
				desiredVel = dir.scalarMult(speed).scalarMult(distance/80);
			}
			else
			{
				desiredVel = dir.scalarMult(speed);					
			}
			
			Position = Position.plus(desiredVel);
		
			if(health < 0)
				isAlive = false;
			
	}
	
	
	public void Paint(float deltaTime, Graphics g)
	{
		matrix.reset();
		matrix.setTranslate((float)Position.x, (float)Position.y);
		matrix.postRotate(rotataion,(float)(Position.x + width/2),(float) (Position.y + height/2));
		g.DrawImagematrix(Assets.bigZombie, matrix);	
		
	}
}
