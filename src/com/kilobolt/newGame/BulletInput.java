package com.kilobolt.newGame;

import android.widget.Button;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class BulletInput 
{
	
	
	Vector2D shotPos;
	Vector2D machinePos;
	Vector2D ak47Pos;
	Vector2D sniperPos;
	int width, height;
	
	boolean Isshot, IsMachine,Isak47, Issniper, ButtonDown;
	
	float timer1,timer2, fixedTime;
	public BulletInput()
	{
		height = 60;
		width = 40;
		shotPos = new Vector2D(750,120);
		machinePos = new Vector2D(750,shotPos.y + height);
		ak47Pos = new Vector2D(750,machinePos.y + height );
		sniperPos = new Vector2D(750, ak47Pos.y+ height);
		Isak47 = true;
		IsMachine = false;
		Isshot = false;
		Issniper = false;
		
		ButtonDown = false;
		timer1 = 0;
		timer2 = 0;
		fixedTime = 0.1f; 
	}
	
	public void Update(float deltaTime, Input input)
	{
		//timer = 0;
		if(PlayerInput.IsSelected(740, 80, 80, 30, input, deltaTime))
		{
			//timer1 += deltaTime/30;
			//if(timer1 > fixedTime)
			//{
				if(!ButtonDown)
					ButtonDown = true;
			
			//}
			
		}
		//else
		//	timer1 = 0;
		
		if(ButtonDown)
		{
		
			if(PlayerInput.IsSelected(740, 360, 80, 30, input, deltaTime))
			{
			//	timer2 += deltaTime/30;
			//	if(timer2 > fixedTime)
				//{				
					ButtonDown = false;			
			//	}
			//
			}
			//else
			//	timer2 = 0;			
		}
		
		if(ButtonDown)
		{
			if(PlayerInput.IsSelected((int)shotPos.x, (int)shotPos.y, width, height, input, deltaTime))
			{
				Isak47 = false;
				IsMachine = false;
				Isshot = true;
				Issniper = false;
			}
			else if(PlayerInput.IsSelected((int)machinePos.x, (int)machinePos.y, width, height, input, deltaTime))
			{
				Isak47 = false;
				IsMachine = true;
				Isshot = false;
				Issniper = false;
			}
			else if(PlayerInput.IsSelected((int)ak47Pos.x, (int)ak47Pos.y, width, height, input, deltaTime))
			{
				Isak47 = true;
				IsMachine = false;
				Isshot = false;
				Issniper = false;
			}
			else if(PlayerInput.IsSelected((int)sniperPos.x, (int)sniperPos.y, width, height, input, deltaTime))
			{
				Isak47 = false;
				IsMachine = false;
				Isshot = false;
				Issniper = true;
			}
			
		}
	}	
	
	public void Paint(float deltaTime, Graphics g)
	{
		g.drawImage(Assets.bulletback, 740, 80);
		if(ButtonDown)
			g.drawImage(Assets.bulletback, 740, 360);
		if(ButtonDown)
		{
		
		if(Isshot)
		{
			g.drawImage(Assets.shotGunSelected,(int) shotPos.x,(int) shotPos.y);
		}
		else
		{	
			g.drawImage(Assets.shotGun,(int) shotPos.x,(int) shotPos.y);
		}
		
		if(IsMachine)
		{
			g.drawImage(Assets.machineGunselected,(int) machinePos.x,(int) machinePos.y);
		}
		else
		{		
			g.drawImage(Assets.machineGun,(int) machinePos.x,(int) machinePos.y);
		}
		
		if(Isak47)
		{
			g.drawImage(Assets.ak47Selected,(int) ak47Pos.x,(int) ak47Pos.y);
		}
		else
		{	
			g.drawImage(Assets.ak47,(int) ak47Pos.x,(int) ak47Pos.y);
		}
		
		if(Issniper)
		{
			g.drawImage(Assets.sniperSelected,(int) sniperPos.x,(int) sniperPos.y);
		}
		else
		{	
			g.drawImage(Assets.sniper,(int) sniperPos.x,(int) sniperPos.y);
		}
		}
	}
}
