package com.kilobolt.newGame;

import java.util.ArrayList;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class ArmyManager
{
	
	public static boolean IsAnyArmySelected;
	static ArrayList<Army> armyList;
	//PlayerGuard pGuard;
	public static boolean overArmy;	
	public static boolean armyPositioningDone = false; 
	
	
	
	
	public ArmyManager()
	{
		IsAnyArmySelected = false;
		
		armyList = new ArrayList<Army>();
		armyList.add(new Army(350, 350,1,"shotgun"));
		armyList.add(new Army(300, 350,2,"machinegun"));
		armyList.add(new Army(450, 350,3,"ak47"));
		armyList.add(new Army(500, 350,4,"sniper"));
		overArmy = false;
		//armyList.get(0).CanFire = true;
		//pGuard = new PlayerGuard(20, 80);
	}
	
	public void Update(float deltaTime, Input input)
	{
		for(int i =0; i < armyList.size(); i++)
		{
			armyList.get(i).Update(deltaTime, input);
		}
		if(!ArmyManager.armyPositioningDone)
		{		
		for(int i =0; i < armyList.size(); i++)
		{
			if(PlayerInput.IsSelected((int)armyList.get(i).Position.x,(int)armyList.get(i).Position.y, 
					armyList.get(i).width, armyList.get(i).height, input,deltaTime))
			{
				if(armyList.get(i).isSelected == true)
					armyList.get(i).isSelected = false;
				else
				{					
					armyList.get(i).isSelected = true;
					for(int j =0; j < armyList.size(); j++)
					{
						if(j != i)
							armyList.get(j).isSelected = false;
					}
				}				
			}			
		}
		
		for(int j =0; j < armyList.size(); j++)
		{
			if(armyList.get(j).isSelected)
				{
					IsAnyArmySelected = true;
					break;
				}
			else
				IsAnyArmySelected = false;
					
		}
		
		}
		for(int i =0; i < armyList.size(); i++)
		{
			if(!PlayerInput.IsSelected((int)armyList.get(i).Position.x,(int)armyList.get(i).Position.y, 
					armyList.get(i).width, armyList.get(i).height, input, deltaTime))
			{
				overArmy =  false;				
			}					
		}
		for(int i =0; i < armyList.size(); i++)
		{
			if(PlayerInput.IsSelected((int)armyList.get(i).Position.x,(int)armyList.get(i).Position.y, 
					armyList.get(i).width, armyList.get(i).height, input, deltaTime))
			{
				overArmy =  true;				
			}					
		}
		
		if(armyPositioningDone)
		{
			 for(int i =0; i < armyList.size(); i++)
			{
				armyList.get(i).isSelected = false;		
				IsAnyArmySelected = false;
			}
		}
	
		//pGuard.Update(deltaTime, input);
	}
		
	
	public void Paint(float deltaTime, Graphics g)
	{
		for(int i =0; i < armyList.size(); i++)
		{
			armyList.get(i).Paint(deltaTime, g);
		}	
		//pGuard.Paint(deltaTime, g);
	}

}
