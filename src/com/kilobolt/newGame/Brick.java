package com.kilobolt.newGame;

import android.graphics.Matrix;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class Brick {
	
	int positionX, positionY, width, height;
	int type;
	int speedX;
	
	public Brick(int posX, int posY, int width, int height, int type, int speed)
	{
		positionX = posX;
		positionY = posY;
		this.width = width;
		this.height = height;
		this.type = type;
		speedX = speed;
		if(speedX < 0)
			speedX -= 1;
		else
			speedX +=1;
		
	}
	
	public void Update(float deltaTime, Input input)
	{	
		
		positionX += speedX;				
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		if(type == 0)
		{
			g.drawImage(Assets.brick1, positionX, positionY);
		}
		else if(type == 1)
		{
			g.drawImage(Assets.brick2, positionX, positionY);
		}
		else if(type == 2)
		{
			g.drawImage(Assets.brick3, positionX, positionY);
		}
	}

}
