
package com.kilobolt.newGame;

import java.util.Vector;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

import android.graphics.Matrix;

public class PlayerGuard
{
	public int height;
	public int width;
	private int speed;
	private Matrix matrix;
	private float rotataion;	
	public Vector2D Position;
	private Vector2D ahead;
	private Vector2D behind;
	
	public PlayerGuard(int pX, int pY)
	{	
		width = 50;
		height = 50;
		
		matrix = new Matrix();
		rotataion = -90;
		Position = new Vector2D(pX,pY);
		speed = 1;
		ahead = new Vector2D();
		behind = new Vector2D();
		
	}
	
	public void Update(float deltaTime, Input input)
	{
		Position = Position.plus(FollowLeader(Player.getInstance().pos, Player.getInstance().vel, 800));
	}
	// player's velocity, distance behind player[float], ahead and behind [vector2], player's position, 
	private Vector2D FollowLeader(Vector2D targetPos, Vector2D targetVel, float DISTBEHIND)
	{
		Vector2D force = new Vector2D();
		targetVel = targetVel.unitVector();
		
		if(targetVel.x == 0)
		{
			targetVel.x = 1;			
		}
		if(targetVel.y == 0)
		{
			targetVel.y = 1;			
		}
		
		targetVel.scalarMult(DISTBEHIND);
		
		//ahead = targetPos.plus(targetVel);
		
		targetVel.scalarMult(-1);
		
		behind.x = targetPos.x + targetVel.x;
		behind.y = targetPos.y +  targetVel.y;
		
		force = force.plus(Arrive(behind, 400));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
			
		return force;
	}
	
	private Vector2D Arrive(Vector2D target, float radius)
	{
		Vector2D force = new Vector2D();
		Vector2D dir = new Vector2D(target.x - Position.x, target.y - Position.y);
		float distance  = (float)dir.length();
		float dAngle = (float)dir.getTheta();
		float diff = dAngle * 180/3.14f  - rotataion;
		rotataion += diff;
		dir = dir.unitVector();
		
		if(distance < radius)
		{
			force = dir.scalarMult(speed).scalarMult(distance/radius);
		}
		else
		{
			force = dir.scalarMult(speed);					
		}
		
		return force;	
	}

	
	public void Paint(float deltaTime, Graphics g)
	{
		matrix.reset();
		matrix.setTranslate((float)Position.x, (float)Position.y);
		matrix.postRotate(rotataion,(float)(Position.x + width/2),(float) (Position.y + height/2));
		g.DrawImagematrix(Assets.army1Selected, matrix);			
	
	}
	
	
	
	
	
}
