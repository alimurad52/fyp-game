package com.kilobolt.newGame;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class EndGameSplashScreen extends Screen{

	
	
	public EndGameSplashScreen(Game game) 
	{
		super(game);
		
		
	}

	float t = 0;	
	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();
		t += deltaTime/60;
		if(input.isTouchDown(0))
		{
			if(t > 1.0f){
				Assets.tapSound.play(5);
					game.setScreen(new Menuscreen(game));				
			}	
		}
	
	}

	
	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		g.drawImage(Assets.endGameScreen,0,0);
		
		
	}
	
	
	

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}
}
