package com.kilobolt.newGame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import android.R.bool;
import android.graphics.Matrix;
import android.graphics.Rect;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Image;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Input.TouchEvent;

public class Player 
{
	int posX, posY,speedX;
	Boolean isMovable =  true;
	float rot;	
	public Vector2D pos;
	public Vector2D vel;
	Matrix matrix;	
	
	ArrayList<Bullet> bulletList;
	float bulletInterval = 0.5f;
	float bulletCoolDown = 0.5f;
	BulletInput bInput;
	String type;
	public static int level = 1;
	public static int score;
	
	private static Player instance;
	public static Player getInstance()
	{
		return instance;
	}
	
	public Player(int pX, int pY, int speed)
	{
		instance = this;
		posX = pX;
		posY = pY;
		pos = new Vector2D(pX,pY);
		vel = new Vector2D(0,0);
		speedX = speed;
		rot = -90;
		matrix = new Matrix();
		bulletList = new ArrayList<Bullet>();
		bInput = new BulletInput();
		type = "shotgun";
		score = 0;
	}
	
	public void Update(float deltaTime, Input input)
	{
		
		if(pos.x > 780)
		{
			isMovable = false;
			posX = 780;
		}
		else if (pos.x < 0)
		{
			isMovable = false;
			posX = 0;
			
		}
		else
			isMovable = true;	
		
		for(int i =0; i< bulletList.size(); i++)
		{
			bulletList.get(i).Update(deltaTime, input);
		}
		bInput.Update(deltaTime, input);
		
		if(bInput.Isak47)
		{
			type = "ak47";			
		}
		else if(bInput.IsMachine)
		{
			type = "machinegun";
		}
		else if(bInput.Issniper)
		{
			type = "sniper";
		}
		else if(bInput.Isshot)
		{
			type = "shotgun";
		}
		

	  	 if (input.isTouchDown(0))
	    	 {			
	    		 if( ArmyManager.IsAnyArmySelected == false)
	    		 {
	    			 if(!ArmyManager.overArmy)
	    			 {
	    				 Vector2D dir = new Vector2D(input.getTouchX(0)- pos.x, input.getTouchY(0) - pos.y);
	    				 float dAngle = (float)dir.getTheta();
	    				 float diff = dAngle * 180/3.14f  - rot;
	    				 rot += diff;
	    				 pos =  new Vector2D(pos.x+ dir.unitVector().scalarMult(3).x,pos.y+ dir.unitVector().scalarMult(3).y); 
	    				 
	    				 
	    				 if(!PlayerInput.upBTDown)
	    				 {
	    				 if(bulletCoolDown >= bulletInterval)
	    				 {
	    					 float angle = ToAngle(new Vector2D(input.getTouchX(0),input.getTouchY(0)));
	    					 Vector2D vel = FromPolar(dAngle , 10);
	    					 Bullet b = new Bullet(pos.x, pos.y,vel,rot,type);
	    					 b.FromPlayer = true;
	    					 bulletList.add(b);
	    					 bulletCoolDown = 0;
	    				 }
	    				 bulletCoolDown += deltaTime/60;
						}
	    			 }
	    		 }			
	    	 }		
	    	 else
	    	 bulletCoolDown = 0.5f;
	     	
		
	     for(int j =0; j< bulletList.size(); j++)
	     {
	    	 if(bulletList.get(j).IsActive == false)
	    	 {
	    		 bulletList.remove(j);
	    	 }
	     }
		
	}	
	
	public void Paint(float deltaTime, Graphics g)
	{
		matrix.reset();
		matrix.setTranslate((float)pos.x,(float)pos.y);
		matrix.postRotate(rot,(float)pos.x + 20, (float)pos.y + 20);
		g.DrawImagematrix(Assets.player,matrix);		
		
		for(int i =0; i< bulletList.size(); i++)
		{
			bulletList.get(i).Paint(deltaTime, g);
		}
		
		bInput.Paint(deltaTime, g);
	}
	
	private Vector2D FromPolar(float angle, float mag)
	{		
		 return new Vector2D(Math.cos(angle)* mag, Math.sin(angle) * mag);
	}
	private float ToAngle(Vector2D vec)
	{		
		 return (float)Math.atan2(vec.y, vec.x);
	}
}
