package com.kilobolt.newGame;

import com.kilobolt.framework.Image;
import com.kilobolt.framework.Music;
import com.kilobolt.framework.Sound;

public class Assets {
	
	public static Image player,background, bulletImage, bulletButton, bulletButtonDown;	
	public static Image left, right, up, down, BTDown;
	public static Image army1, army1Selected, zombie;
	public static Image brick1,brick2,brick3, topJungleImage,inst,ttc,level3Splsh, endGameScreen;
	public static Image bulletback, shotGun, shotGunSelected, machineGun, machineGunselected, ak47, ak47Selected, sniper, sniperSelected; 
	public static Image menuScreen,level1Splash,level2Splash,pauseBT,resumeScreen,goScreen,bigZombie;
	public static Image level2back, level2backTop,zom,vir,sur,city,zombieMed, level3Back, level3Top, leve3House,level2House;
	public static Sound tapSound,bulletSound, zombiekillSound;
	public static Music amb,music1,music2, tr1,tr2,tr3,tr4;
}
