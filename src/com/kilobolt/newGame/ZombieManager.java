package com.kilobolt.newGame;

import java.util.ArrayList;
import java.util.Random;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class ZombieManager 
{
	public static ArrayList<Zombie> zombieList;
	public static ArrayList<ZombieMed> zombiemedList;
	Random rand = new Random();
	public static ZombieManager _instance;
	public boolean go = false;
	public ArrayList<Vector2D> positions;
	public ArrayList<Vector2D> positionsMed;
	public ZombieMed1 bigZombie;
	
	
	
	public ZombieManager(ArrayList<Vector2D> pos)
	{
		_instance = this;
		zombieList = new ArrayList<Zombie>();
		zombiemedList = new ArrayList<ZombieMed>();
		positions = pos;
		//zombieList.add(new Zombie(rand.nextInt(700) + 20, 0,rand.nextInt(gradualSpeed)+ 1));
		positionsMed = new ArrayList<Vector2D>();
		/*positionsMed.add(new Vector2D(145,0));
		positionsMed.add(new Vector2D(295,0));
		positionsMed.add(new Vector2D(485,0));*/
		bigZombie = new ZombieMed1(370, 0,0.5f);
	}
	
	
	float t =0;
	float t1 =0;
	int gradualSpeed = 1;
	float tForSpeed = 0;
	float zombieLifetime = 0;
	float yay = 2;
	public boolean noMoreZombie = false;
	
	public boolean noMoreBigZombie = false;
	
	public void Update(float deltaTime, Input input)
	{		
		
		t += deltaTime/60;
		if(t > yay)
		{
			if(zombieLifetime <= 20)//change this to reduce the gameplay time
			{
				Vector2D tempPos = positions.get(rand.nextInt(positions.size()));
				zombieList.add(new Zombie((int)tempPos.x,(int)tempPos.y, rand.nextInt(gradualSpeed)+ 1));
			}
			
			
			
				
	
			t = 0;
		}
		
		
		if(Player.level == 2 || Player.level == 3)
		{
			t1 += deltaTime/60;
			if(t1 > 5)
			{
				
			if(zombieLifetime <= 20)//change this to reduce the gameplay time
			{
				Vector2D tempPos = positionsMed.get(rand.nextInt(positionsMed.size()));
				zombiemedList.add(new ZombieMed((int)tempPos.x,(int)tempPos.y, rand.nextInt(gradualSpeed)+ 1));
			}
			
			t1 = 0;
			}
		}
		
		
		
		if(zombieLifetime > 20)//change this to reduce the gameplay time
		{
			if(Player.level == 1)
			{
				if(zombieList.size() == 0)
				noMoreZombie = true;
			}			
			
			if(Player.level == 2 || Player.level == 3)
			{
				if(zombieList.size() == 0 && zombiemedList.size() == 0)
				noMoreZombie = true;
			}	
			
			
		}
		
		if(Player.level == 3 && noMoreZombie)
		{
			bigZombie.Update(deltaTime, input);
			
			
				
		}
		
		
		if(yay > 0.8f)
			yay -= 0.001f;
		
		tForSpeed += deltaTime/60;
		if(tForSpeed > 30)
		{
			if(gradualSpeed < 8)
			{
				gradualSpeed++;
			}
			tForSpeed = 0;
		}
	
		
		for(int i =0; i < zombieList.size(); i++)
		{
			zombieList.get(i).Update(deltaTime, input);
		}
		
		for(int i =0; i < zombieList.size(); i++)
		{
			if(!zombieList.get(i).isAlive)
			{
				Assets.zombiekillSound.play(5);
				zombieList.remove(i);
			}
		}
		
		if(Player.level == 2 || Player.level == 3)
		{
		for(int i =0; i < zombiemedList.size(); i++)
		{
			zombiemedList.get(i).Update(deltaTime, input);
		}
		
		for(int i =0; i < zombiemedList.size(); i++)
		{
			if(!zombiemedList.get(i).isAlive)
			{
				Assets.zombiekillSound.play(5);
				zombiemedList.remove(i);
			}
		}
		}
		
		
		if(ArmyManager.armyPositioningDone)
		zombieLifetime += deltaTime/60;
		
		for(int i =0; i < zombieList.size(); i++)
		{
			if(zombieList.get(i).Position.y > 420)
			{
				go = true;
			}
		}
		
		if(Player.level == 2 || Player.level == 3)
		{
		
		for(int i =0; i < zombiemedList.size(); i++)
		{
			if(zombiemedList.get(i).Position.y > 420)
			{
				go = true;
			}
		}
		
		}
		
		if(!bigZombie.isAlive)
			ZombieManager._instance.noMoreBigZombie = true;
	
	}	
	
	
	public void Paint(float deltaTime, Graphics g)
	{
		for(int i =0; i < zombieList.size(); i++)
		{
			zombieList.get(i).Paint(deltaTime, g);
		}
		
		if(Player.level == 3 && noMoreZombie)
		{
			if(bigZombie.isAlive)
				bigZombie.Paint(deltaTime, g);
		}
		
		if(Player.level == 2 || Player.level == 3)
		{
		for(int i =0; i < zombiemedList.size(); i++)
		{
			zombiemedList.get(i).Paint(deltaTime, g);
		}
		}
	}
}
