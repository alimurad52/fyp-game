package com.kilobolt.newGame;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Debug;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;

public class Bullet 
{
	public Vector2D position;
	public Vector2D vel;
	public float rot;
	Matrix matrix;	
	String type;
	public boolean IsActive;
	float timer;
	public Rect rect;
	Boolean FromPlayer = false;
	
	Bullet( double pX, double pY, Vector2D velocity, float rotation, String type)
	{
		position = new Vector2D(pX + velocity.x * 2,pY + velocity.y * 2);
		vel = velocity;  
		rot = rotation;
		matrix = new Matrix();
		IsActive = true;
		timer = 0;
		this.type = type;
		rect = new Rect((int)position.x, (int)position.y,20, 20);
		Assets.bulletSound.play(0.2f);
	}
	public void Update(float deltaTime, Input input)
	{	
		position.x += vel.x;
		position.y += vel.y;
		timer += deltaTime/30;
		rect = new Rect((int)position.x, (int)position.y,(int)position.x + 10, (int)position.y + 10);
		
		if(type == "shotgun")
		{
			if(timer >= 0.5f)
			{
				IsActive = false;
			}
		}
		else if(type == "machinegun")
		{
			if(timer >= 0.8f)
			{
				IsActive = false;
			}
		}
		else if(type == "ak47")
		{
			if(timer >= 1.5f)
			{
				IsActive = false;
			}
			
		}
		else if(type == "sniper")
		{
			if(timer >= 2.5f)
			{
				IsActive = false;
			}
		}
		
		/*else
		{
			if(timer >= 2.5f)
			{
				IsActive = false;
			}
		}*/
		
		for(int j =0; j< ZombieManager._instance.zombieList.size(); j++)
		{
			if(rect.intersect(ZombieManager._instance.zombieList.get(j).rect))
			{
				if(FromPlayer)
					Player.score += 10;
				Assets.zombiekillSound.play(5);
				ZombieManager._instance.zombieList.remove(j);
				IsActive = false;
			}
		}
		
		if(Player.level == 2 || Player.level == 3)
		{
		
		for(int j =0; j< ZombieManager._instance.zombiemedList.size(); j++)
		{
			if(rect.intersect(ZombieManager._instance.zombiemedList.get(j).rect))
			{
				if(FromPlayer)
					Player.score += 5;
				//Assets.zombiekillSound.play(5);
				ZombieManager._instance.zombiemedList.get(j).health -= 20;
				IsActive = false;
			}
		}
		}
		
		if(Player.level == 3)
		{
			if(rect.intersect(ZombieManager._instance.bigZombie.rect))
			{
				if(FromPlayer)
					Player.score += 10;
				//Assets.zombiekillSound.play(5);
				IsActive = false;
				ZombieManager._instance.bigZombie.health -= 5;
				
			}
		}
		
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		matrix.reset();
		matrix.setTranslate((float)position.x,(float)position.y);
		matrix.postRotate(rot,(float)position.x + 5, (float)position.y + 5);
		
		
		g.DrawImagematrix(Assets.bulletImage,matrix);		
		//g.drawImage(Assets.bulletImage,(int) position.x,(int) position.y);
	}
}
