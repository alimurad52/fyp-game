package com.kilobolt.newGame;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public class Menuscreen extends Screen{

	
	
	public Menuscreen(Game game) 
	{
		super(game);
		
		
	}

	@Override
	public void update(float deltaTime) 
	{
		Input input = game.getInput();

		if(input.isTouchDown(0))
		{			
			if( input.getTouchX(0) > 118 && input.getTouchX(0) < 271)
			{
				if(input.getTouchY(0) > 148 && input.getTouchY(0) < 190)
				{
					Assets.tapSound.play(5);
					game.setScreen(new Story(game));    //game.setScreen(new level1Splash(game));
				}
			}
			
			if( input.getTouchX(0) > 118 && input.getTouchX(0) < 271)
			{
				if(input.getTouchY(0) > 265 && input.getTouchY(0) < 305)
				{
					Assets.tapSound.play(5);
				}
			}
			
			if( input.getTouchX(0) > 118 && input.getTouchX(0) < 271)
			{
				if(input.getTouchY(0) > 384 && input.getTouchY(0) < 424)
				{
					Assets.tapSound.play(5);
					System.exit(0);
				}
			}
			
			
			
			
		}
	
	}

	
	@Override
	public void paint(float deltaTime) {
		
		Graphics g = game.getGraphics();
		g.clearScreen(2345);
		g.drawImage(Assets.menuScreen,0,0);
		
		
	}
	
	
	

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resume() {
	
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub
		
	}
}


