package com.kilobolt.newGame;

import java.util.ArrayList;

import android.R.string;
import android.graphics.Matrix;
import android.graphics.Point;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;


public class Army 
{
	
	public int height;
	public int width;
	private int speed;
	private Matrix matrix;
	private float rotataion;	
	public Vector2D Position;
	private Vector2D targetPosition;
	private Vector2D desiredVel;
	public boolean isSelected;
	private Boolean hasTargetPos;
	private int ArmyNumber;
	private String gunType;
	public boolean CanFire = false;
	
	ArrayList<Bullet> bulletList;
	float bulletInterval = 0.5f;
	float bulletCoolDown = 0.5f;
	boolean isFiring = false;
	
	public Army(int pX, int pY, int num, String type )
	{	
		width = 50;
		height = 50;
		
		matrix = new Matrix();
		rotataion = -90;
		Position = new Vector2D(pX,pY);
		desiredVel = new Vector2D();
		isSelected = false;
		targetPosition = null;
		hasTargetPos = false;
		speed = 1;
		ArmyNumber = num;
		gunType = type;
		bulletList = new ArrayList<Bullet>();
		
		if(type == "shotgun")
		{
			bulletInterval = 2.0f;
		}
		else if(type == "machinegun")
		{
			bulletInterval = 0.2f;
		}
		else if(type == "ak47")
		{
			bulletInterval = 0.6f;			
		}
		else if(type == "sniper")
		{
			bulletInterval = 2.8f;
		}
		
	}
	
	boolean select = false;
	public void Update(float deltaTime, Input input)
	{
	
		for(int i =0; i< bulletList.size(); i++)
		{
			bulletList.get(i).Update(deltaTime, input);
			if(bulletList.get(i).IsActive == false)
			{
				bulletList.remove(i);
			}
		}
		
		for(int i =0; i< bulletList.size(); i++)
		{
			if(bulletList.get(i).IsActive == false)
			{
				bulletList.remove(i);
			}
		}
		
		if(!ArmyManager.armyPositioningDone)
		{
		if(isSelected)
		{
			if(input.isTouchDown(0))
			{
				if(!PlayerInput.IsSelected((int)Position.x,(int) Position.y, width, height, input,deltaTime))
				{
					for(int i =0 ; i < ArmyManager.armyList.size(); i++)
					{
						if(ArmyManager.armyList.get(i) != this)
						{						
						if(PlayerInput.IsSelected((int)ArmyManager.armyList.get(i).Position.x,(int) ArmyManager.armyList.get(i).Position.y, width,height, input, deltaTime))
						{
							select = true;
							break;
						}
						else
							select = false;
						}
					}
						
					if(select == false)
					{
						targetPosition = new Vector2D(input.getTouchX(0),input.getTouchY(0));
						hasTargetPos = true;
					}
				}
			}
			
		}
		
		if(hasTargetPos)
		{
			Vector2D dir = new Vector2D(targetPosition.x - Position.x, targetPosition.y - Position.y);
			float distance  = (float)dir.length();
			float dAngle = (float)dir.getTheta();
			float diff = dAngle * 180/3.14f  - rotataion;
			rotataion += diff;
			dir = dir.unitVector();
			
			if(distance < 80)
			{
				desiredVel = dir.scalarMult(speed).scalarMult(distance/80);
			}
			else
			{
				desiredVel = dir.scalarMult(speed);					
			}
			
			Position = Position.plus(desiredVel);
		}
		}
		
		
		Zombie  nearest = getNearestZombie();
		if(nearest != null)
		{
			float dist = calculateDistance(nearest.Position, this.Position);
			if(dist < 100)
			{
				faceEnemy(nearest);
				isFiring = true;
				if(bulletCoolDown >= bulletInterval)
				{
					Vector2D dir = new Vector2D(nearest.Position.x - Position.x, nearest.Position.y - Position.y);
					float dAngle = (float)dir.getTheta();
					Vector2D vel = FromPolar(dAngle , 10);
					bulletList.add(new Bullet(Position.x, Position.y,vel,rotataion,gunType));
					bulletCoolDown = 0;
				}
				bulletCoolDown += deltaTime/60;				
				
			}
		}
		else
			isFiring = false;
		
		
		for(int i =0; i< bulletList.size(); i++)			
		{
		
		for(int j =0; j< ZombieManager._instance.zombieList.size(); j++)
		{
			if(bulletList.get(i).rect.intersect(ZombieManager._instance.zombieList.get(j).rect))
			{
				ZombieManager._instance.zombieList.remove(j);
				bulletList.get(i).IsActive = false;
			}
		}
		}
		
	}
	
	private void faceEnemy(Zombie nearest)
	{
		Vector2D dir = new Vector2D(nearest.Position.x - Position.x, nearest.Position.y - Position.y);
		float dAngle = (float)dir.getTheta();
		float diff = dAngle * 180/3.14f  - rotataion;
		rotataion += diff;
	}
	
	
	
	private Zombie getNearestZombie()
	{
		Zombie nearest = null;
		float shortestDist = 200;
		float distance;
		
		for(int i =0; i< ZombieManager._instance.zombieList.size(); i++)
		{
			distance = calculateDistance(ZombieManager._instance.zombieList.get(i).Position, this.Position); 
			if(distance <= 100 && distance < shortestDist)
			{
				shortestDist = distance;
				nearest = ZombieManager._instance.zombieList.get(i);
			}
		}
		
		return nearest;
	}
	
	private float calculateDistance(Vector2D a, Vector2D b)
	{
		return (float)Math.sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
	}
	
	public void Paint(float deltaTime, Graphics g)
	{
		matrix.reset();
		matrix.setTranslate((float)Position.x, (float)Position.y);
		matrix.postRotate(rotataion,(float)(Position.x + width/2),(float) (Position.y + height/2));
		if(isSelected)
		{
			g.DrawImagematrix(Assets.army1Selected, matrix);			
		}
		else
		{
			g.DrawImagematrix(Assets.army1, matrix);			
		}
		
		for(int i =0; i< bulletList.size(); i++)
		{
			bulletList.get(i).Paint(deltaTime, g);
		}
		
		/*
		
		if(ArmyNumber == 1)
		{		
			if(isSelected)
			{
				g.DrawImagematrix(Assets.army1Selected, matrix);			
			}
			else
			{
				g.DrawImagematrix(Assets.army1, matrix);			
			}
		}
		else if(ArmyNumber == 2)
		{
			if(isSelected)
			{
				g.DrawImagematrix(Assets.army1Selected, matrix);			
			}
			else
			{
				g.DrawImagematrix(Assets.army1, matrix);			
			}			
		}
		else if(ArmyNumber == 3)	
		{
			if(isSelected)
			{
				g.DrawImagematrix(Assets.army1Selected, matrix);			
			}
			else
			{
				g.DrawImagematrix(Assets.army1, matrix);			
			}			
		}
		else if(ArmyNumber == 4)
		{
			if(isSelected)
			{
				g.DrawImagematrix(Assets.army1Selected, matrix);			
			}
			else
			{
				g.DrawImagematrix(Assets.army1, matrix);			
			}			
		}
		*/
	}
	
	private Vector2D FromPolar(float angle, float mag)
	{		
		 return new Vector2D(Math.cos(angle)* mag, Math.sin(angle) * mag);
	}
	private float ToAngle(Vector2D vec)
	{		
		 return (float)Math.atan2(vec.y, vec.x);
	}
}
	